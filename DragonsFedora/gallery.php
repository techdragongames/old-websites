<DOCTYPE! html>
    <head>
        <title>Dragon's Fedora | Gallery</title>
        <meta charset="utf-8">
        <meta name="description" content="Portfolio Websites and Game Developer">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="styles/gallery.css" type="text/css">
    </head>
<body>

<section class="header">

    <a href="index.php"><img class="header-logo" src="images/topLogo.png"></a>

    <nav class="header-nav">

        <ul class="nav">
            <li><a href="https://github.com/Dragons-Fedora">GitHub</a></li>
            <li><a href="https://twitter.com/Dragons_Fedora">Twitter</a></li>
            <li><a href="https://plus.google.com/u/2/117909765374870212697">Google+</a></li>
            <li><a id="link" href="#about">About</a></li>
            <li><a id="link" href="#work">Work</a></li>
            <li><a href="#contact">Contact</a></li>
        </ul>

    </nav>

</section>


    <div id="main">

        <a href="/images/gallery/'Open_Window'_polyscape_wallpaper.png"><div id="content_2_fixed">
                <h3>P o l y S c a p e   W a l l p a p e r</h3>
        </div></a>
        <a href="/images/gallery/Low%20Poly%20World.png"><div id="content_1">
                <h3>L o w  P o l y  W o r l d</h3>
            </div></a>
        <a href="/images/gallery/Dragon's_Fedora_Wallpaper.png"><div id="content_4_fixed">
                <h3>D r a g o n ' s   F e d o r a  W a l l p a p e r</h3>
            </div></a>
        <a href="/images/gallery/Window$_$ucks.png"><div id="content_3">
                <h3>W i n d o w $   $ u c k s   w a l l p a p  e r</h3>
            </div></a>
        <a href="/images/gallery/Low_Poly_World_Wallpaper.png"><div id="content_5_fixed">
                <h3>L o w   P o l y   W a l l p a p e r</h3>
            </div></a>


    </div>

</body>