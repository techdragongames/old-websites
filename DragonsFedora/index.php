<!DOCTYPE html>

<head>

    <title>Dragon's Fedora</title>
    <meta charset="utf-8">
    <meta name="description" content="One-Page Websites">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="styles/main.css" type="text/css">

</head>

<body>

    <section class="header">

        <a href="index.php"><img class="header-logo" src="images/topLogo.png"></a>

        <nav class="header-nav">

            <ul class="nav">
                <li><a href="https://github.com/Dragons-Fedora">GitHub</a></li>
                <li><a href="https://twitter.com/Dragons_Fedora">Twitter</a></li>
                <li><a href="https://plus.google.com/u/2/117909765374870212697">Google+</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a id="link" href="#about">About</a></li>
                <li><a id="link" href="#work">Work</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>

        </nav>

    </section>

    <div class="topHeader">

        <div class="video">
            <video poster="images/topHeader.jpg" autoplay="true" loop>
            <source src="vids/Background.mp4" type="video/mp4">
        </video>
        </div>

        <h1 class="headerTitle">Dragon's Fedora</h1>

    </div>

    <!-- About Section -->

    <div class="aboutme">

        <h4 class="aboutme">
            <a name="about"></a>About Me</h4>

        <div class="contentAbout">

            <p class="aboutme">My name is Callum and I am currently studying a level 3 BTEC in IT. I am hoping to go university and study game development. I hope to one day to own a games development company. I also have a strong passion for all things Linux, I love customizing different distributions and browsing through the source code for some projects I like.
            </p>

            <img class="aboutme" src="images/topLogo.png" height="240px">

        </div>
    </div>
    <!-- End of About -->

    <!-- Work Section -->

    <div class="work">

        <h4 class="workTitle">
            <a name="work"></a>My Work</h4>

        <div class="workImages">

            <a name="work" href="http://techdragonsoft.deviantart.com/art/Low-Poly-World-Final-589925059"><img src="images/work/workLowPolyWorld.jpg" alt="Low Poly World Art" class="workImage"></a>

            <a name="work" href="http://www.dragonsfedora.esy.es"><img src="images/work/workPortfolioWebsite.jpg" alt="My Portfolio Website" class="workImage"></a>

            <a name="work" href="http://www.takinglevelsserver.esy.es/"><img src="images/work/workTakingLevels.jpg" alt="My Portfolio Website" class="workImage"></a>

            <a name="work" href="https://plus.google.com/u/1/117909765374870212697"><img src="images/work/workLinuxPorn.jpg" alt="Eye Candy" class="workImage">


        </div>

    </div>

    <!-- End of Work -->


    <!-- Contact Section -->
    <div class="contact">

        <h4 class="contact" style="color: black;">
            <a name="contact"></a>Contact Me</h4>

        <script type="text/javascript" defer src="//www.123contactform.com/embed/2479741.js" data-role="form"></script>
        <p>
            <a class="footerLink13" title="123ContactForm" href="http://www.123contactform.com"></a>
            <a style="font-size:small!important;color:#000000!important; text-decoration:underline!important;" title="Looks like phishing? Report it!" href="http://www.123contactform.com/sfnew.php?s=123contactform-52&control119314=http:///contact-form--2479741.html&control190=Report%20abuse" rel="nofollow"></a>
        </p>


    </div>
    <!-- End of Contact -->

</body>
